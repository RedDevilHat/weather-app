
# Weether APP

Небольшой проект собирающий информацию из рахных источников данных о погоде.


## Installation

Для запуска проекта. Клонируем его.

```bash
  make docker-override-dev
  make config-override 
```

Запоняем различные данные в конфигах (токены, порты)


```bash
  make init
```   

После данной команды, проект собирется, и будет готов к исползованию.
## API Reference

#### Получаем погоду

```http
  GET /api/weather
```

| Query string parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `city` | `string` | **Required**. Название города или населенного пункта для которого ищем статситику |


#### Получаем данные за день по статистике запросов

```http
  GET /api/city-stats/for-one-day
```

| Query string parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `date` | `string` | **Required**. День для сбора статистики. Формат `Y-m-d` |

```http
  GET /api/city-stats/for-range
```

| Query string parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `dateStart` | `string` | **Required**. День старта сбора статистики. Формат `Y-m-d` |
| `dateEnd` | `string` | **Required**. День окончания сбора статистики. Формат `Y-m-d` |

## FAQ

#### Как происходит сбор статистики

В лоб. Простая job'a создает в БД запись вида cityName + created_at

а контроллер вызывает историю которая просто выполняет groupBy запрос в БД с  лимитом в 5 сообщейний (есть возможность вытащить в апи параметр.)

Для серьезного проекта вариант не годится. Минимально надо накинуть индекс на дату и имя города.
А еще лучше подобрать какуюнибудь БД получше, сделать крон команду которая раз в N времени будет собирать сводную таблицу, а из нее уже в апи отдавать.


#### Как добавить новый источник данных для API

Надо:

1) Реализовать класс в папочке app/Clients имплементирующий интерфейс ApiClientInterface
2) Реализовать класс Loader в app/DataLoaders наследующий AbstractApiLoader
3) В историю GetWeatherInAllSystem добавить в зависимости новый класс по образу и подобию

```php
    public function __construct(
        M3OApiLoader $m3OApiLoader,
        OpenWeatherMapApiLoader $openWeatherMapApiLoader,
        WeatherApiLoader $weatherApiLoader
    )
    {
        $this->receivers = [
            $m3OApiLoader,
            $openWeatherMapApiLoader,
            $weatherApiLoader,
        ];
    }
```

#### Проблемы проекта

1) Не настроен кеш. Вообще, с запросом сразу идем в АПИ сторонней системы.
2) N запросов выполнняются последовательно. Весьма долгий ответ может получится. Надо бы вынести примерно вот так

```php
$responses = Http::pool(fn (Pool $pool) => [
    $pool->get('http://localhost/first'),
    $pool->get('http://localhost/second'),
    $pool->get('http://localhost/third'),
]);
```

Но придется немного лоадеры переделать. И Историю.
## Running Tests

Для тестов запустить

```bash
  make test
```


## Contributing

Правила разработки.
1) Писать тесты.
2) Запускать тесты.
3) выполнять make cs-fixer перед коммитом.

*а тут неплохо хуки настроить на тесты и --dry-run php-cs-fixer* 
