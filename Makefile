PHP_SERVICE := php

docker-override-dev:
	@cp .docker/php/dev/Dockerfile.dist .docker/php/dev/Dockerfile

config-override:
	@cp docker-compose.override.yaml.dist docker-compose.override.yaml
	@cp .env.example .env

build:
	@docker-compose build

up:
	@docker-compose up -d

composer:
	@docker-compose exec -T $(PHP_SERVICE) composer install

db:
	@docker-compose exec -T $(PHP_SERVICE) ./artisan migrate

clean:
	@docker-compose down --rmi local --remove-orphans

test:
	@docker-compose exec -T $(PHP_SERVICE) vendor/bin/phpunit

stop:
	@docker-compose stop

cs-fixer:
	@docker-compose exec -T $(PHP_SERVICE) vendor/bin/php-cs-fixer fix app

#FOR DEV ONLY
fix-premission:
	@docker-compose exec -T $(PHP_SERVICE) chmod -R 777 storage/

init:
	@make build
	@make up
	@make composer
	@make db
	@make fix-premission
