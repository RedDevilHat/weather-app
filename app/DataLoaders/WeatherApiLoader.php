<?php

namespace App\DataLoaders;

use App\Clients\WeatherApiClient;
use App\DTO\WeatherStats;
use App\Exceptions\WeatherParsingException;
use Illuminate\Http\Client\Response;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class WeatherApiLoader extends AbstractApiLoader
{
    public const SOURCE = 'weatherapi.com';

    public function __construct(WeatherApiClient $client)
    {
        $this->client = $client;
    }

    public function transform(Response $response): WeatherStats
    {
        $propertyAccessor = new PropertyAccessor();

        $arrayResponse = $response->json();

        $city = $propertyAccessor->getValue($arrayResponse, '[location][name]');
        $temp = $propertyAccessor->getValue($arrayResponse, '[current][temp_c]');

        if (null === $city || null === $temp) {
            throw new WeatherParsingException();
        }

        return new WeatherStats($city, (float) $temp, self::SOURCE);
    }
}
