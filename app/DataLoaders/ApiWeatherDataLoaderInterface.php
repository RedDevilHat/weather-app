<?php

namespace App\DataLoaders;

use App\DTO\WeatherStats;
use Illuminate\Http\Client\Response;

interface ApiWeatherDataLoaderInterface
{
    public function load(string $cityName): WeatherStats;

    public function transform(Response $response): WeatherStats;
}
