<?php

namespace App\DataLoaders\ModelLoaders;

use App\Models\RequestLog;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class RequestLogsLoader
{
    public function loadByDateRange(Carbon $from, Carbon $to, int $limit = 5): Collection
    {
        $subQuery = DB::table(RequestLog::TABLE_NAME)
            ->select('city', DB::raw('count(*) as total'))
            ->groupBy('city');

        if ($from !== $to) {
            $subQuery->whereBetween('created_at', [$from->format('Y-m-d'), $to->format('Y-m-d')]);
        }

        if ($from > $to) {
            throw new \LogicException('Date to must be greater than from');
        }

        if ($from === $to) {
            $subQuery->where(DB::raw("to_char(created_at, 'YYYY-MM-DD')"), '=', $from->format('Y-m-d'));
        }

        return DB::table(DB::raw(sprintf("(%s) as result", $subQuery->toSql())))
            ->mergeBindings($subQuery)
            ->orderBy('result.total', 'DESC')
            ->limit($limit)
            ->get()
            ;
    }
}
