<?php

namespace App\DataLoaders;

use App\Clients\M3OApiClient;
use App\DTO\WeatherStats;
use App\Exceptions\WeatherParsingException;
use Illuminate\Http\Client\Response;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class M3OApiLoader extends AbstractApiLoader
{
    public const SOURCE = 'm3o.com';

    public function __construct(M3OApiClient $client)
    {
        $this->client = $client;
    }

    public function transform(Response $response): WeatherStats
    {
        $propertyAccessor = new PropertyAccessor();

        $arrayResponse = $response->json();

        $city = $propertyAccessor->getValue($arrayResponse, '[location]');
        $temp = $propertyAccessor->getValue($arrayResponse, '[temp_c]');

        if (null === $city || null === $temp) {
            throw new WeatherParsingException();
        }

        return new WeatherStats($city, (float) $temp, self::SOURCE);
    }
}
