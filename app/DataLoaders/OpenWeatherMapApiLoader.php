<?php

namespace App\DataLoaders;

use App\Clients\OpenWeatherMapApiClient;
use App\DTO\WeatherStats;
use App\Exceptions\WeatherParsingException;
use Illuminate\Http\Client\Response;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class OpenWeatherMapApiLoader extends AbstractApiLoader
{
    public const SOURCE = 'openweathermap.org';

    public function __construct(OpenWeatherMapApiClient $client)
    {
        $this->client = $client;
    }

    public function transform(Response $response): WeatherStats
    {
        $propertyAccessor = new PropertyAccessor();

        $arrayResponse = $response->json();

        $city = $propertyAccessor->getValue($arrayResponse, '[name]');
        $temp = $propertyAccessor->getValue($arrayResponse, '[main][temp]');

        if (null === $city || null === $temp) {
            throw new WeatherParsingException();
        }

        return new WeatherStats($city, (float) $temp, self::SOURCE);
    }
}
