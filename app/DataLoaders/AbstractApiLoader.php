<?php

namespace App\DataLoaders;

use App\Clients\ApiClientInterface;
use App\DTO\WeatherStats;
use App\Exceptions\WeatherGettingException;
use App\Exceptions\WeatherParsingException;
use Illuminate\Http\Client\Response;

abstract class AbstractApiLoader implements ApiWeatherDataLoaderInterface
{
    public const SOURCE = 'no-source';
    protected ApiClientInterface $client;

    /**
     * Загружает информацию по погоде.
     *
     * @throws WeatherGettingException
     * @throws WeatherParsingException
     */
    public function load(string $cityName): WeatherStats
    {
        $response = $this->client->request($cityName);

        if ($response->failed()) {
            throw new WeatherGettingException();
        }

        return $this->transform($response);
    }

    /**
     * Кастуем данные из сторонней системы, в наше DTO.
     *
     * @throws WeatherParsingException;
     */
    abstract public function transform(Response $response): WeatherStats;
}
