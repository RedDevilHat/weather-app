<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestLog extends Model
{
    use HasFactory;
    public const TABLE_NAME = 'weather_api_requests_logs';
    protected $table = 'weather_api_requests_logs';

    /**
     * @var string[]
     */
    protected $fillable = [
        'city',
    ];
}
