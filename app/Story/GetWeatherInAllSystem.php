<?php

namespace App\Story;

use App\DataLoaders\AbstractApiLoader;
use App\DataLoaders\M3OApiLoader;
use App\DataLoaders\OpenWeatherMapApiLoader;
use App\DataLoaders\WeatherApiLoader;
use App\DTO\WeatherSummary;
use App\Exceptions\WeatherGettingException;
use App\Exceptions\WeatherNotFoundException;
use App\Exceptions\WeatherParsingException;
use App\Utils\GeneratorHelper;
use Illuminate\Support\Facades\Log;

class GetWeatherInAllSystem
{
    private array $receivers;

    public function __construct(
        M3OApiLoader $m3OApiLoader,
        OpenWeatherMapApiLoader $openWeatherMapApiLoader,
        WeatherApiLoader $weatherApiLoader
    )
    {
        $this->receivers = [
            $m3OApiLoader,
            $openWeatherMapApiLoader,
            $weatherApiLoader,
        ];
    }

    /**
     * @throws WeatherNotFoundException
     */
    public function getSummary(string $cityName): WeatherSummary
    {
        $results = [];
        $tempsArray = [];
        /** @var AbstractApiLoader $reciver */
        foreach (GeneratorHelper::genereted($this->receivers) as $reciver) {
            try {
                $currentWeather = $reciver->load($cityName);
                $results[] = $currentWeather;
                $tempsArray[] = $currentWeather->getTemp();
            } catch (WeatherGettingException | WeatherParsingException $exception) {
                Log::error(sprintf('[%s] %s', $reciver::SOURCE, $exception->getMessage()));
                continue;
            } catch (\Throwable $exception) {
                Log::critical(sprintf('[%s] %s', $reciver::SOURCE, $exception->getMessage()));
                continue;
            }
        }

        if (count($results) === 0) {
            throw new WeatherNotFoundException();
        }

        $averaged = $this->calculateAverage($tempsArray);

        return new WeatherSummary($results, $averaged);
    }

    private function calculateAverage(array $temps): float
    {
        array_filter($temps, static fn ($item) => is_float($item));
        $count = count($temps);

        if (0 === $count) {
            throw new WeatherNotFoundException('Weather data is corrupted. Cannot calculate average');
        }

        return array_sum($temps) / $count;
    }
}
