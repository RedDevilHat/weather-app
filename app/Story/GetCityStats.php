<?php

namespace App\Story;

use App\DataLoaders\ModelLoaders\RequestLogsLoader;
use App\DTO\CityStats;
use Carbon\Carbon;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class GetCityStats
{
    private RequestLogsLoader $loader;

    public function __construct(RequestLogsLoader $loader)
    {
        $this->loader = $loader;
    }

    /**
     * @return CityStats[]
     */
    public function forOneDay(Carbon $day): array
    {
        return $this->forRange($day, $day);
    }

    /**
     * @return CityStats[]
     */
    public function forRange(Carbon $dateStart, Carbon $dateStop): array
    {
        if ($dateStart > $dateStop) {
            throw new \LogicException('Date to must be greater than from');
        }

        $data = $this->loader->loadByDateRange($dateStart, $dateStop);
        $propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
            ->disableExceptionOnInvalidPropertyPath()
            ->getPropertyAccessor();

        $result = [];

        foreach ($data as $item) {
            $city = $propertyAccessor->getValue($item, 'city');
            $total = $propertyAccessor->getValue($item, 'total');

            if (!$city || !$total) {
                throw new \LogicException('Corrupted database data');
            }

            $result[] = new CityStats(
                cityName: $city,
                countOfRequest: $total
            );
        }

        return $result;
    }
}
