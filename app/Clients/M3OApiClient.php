<?php

namespace App\Clients;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class M3OApiClient implements ApiClientInterface
{
    public function request(string $cityName): Response
    {
        try {
            return Http::withToken(env('M30_API_TOKEN'))
                ->withBody(json_encode(['location' => $cityName], JSON_THROW_ON_ERROR), 'application/json')
                ->post(
                    sprintf('%s/v1/weather/Now', env('M30_API_BASE_URI'))
                );
        } catch (\JsonException $exception) {
            throw new \LogicException(message: 'Cannot create request', previous: $exception);
        }
    }
}
