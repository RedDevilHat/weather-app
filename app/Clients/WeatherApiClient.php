<?php

namespace App\Clients;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class WeatherApiClient implements ApiClientInterface
{
    public function request(string $cityName): Response
    {
        return Http::get(sprintf('%s/v1/current.json', env('WEATHER_API_BASE_URI')), [
            'key' => env('WEATHER_API_KEY'),
            'q' => $cityName,
        ]);
    }
}
