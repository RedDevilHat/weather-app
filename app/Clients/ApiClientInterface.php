<?php

namespace App\Clients;

use Illuminate\Http\Client\Response;

interface ApiClientInterface
{
    public function request(string $cityName): Response;
}
