<?php

namespace App\Clients;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class OpenWeatherMapApiClient implements ApiClientInterface
{
    public function request(string $cityName): Response
    {
        return Http::get(sprintf('%s/data/2.5/weather', env('OPEN_WEATHER_MAP_API_BASE_URI')), [
            'appid' => env('OPEN_WEATHER_MAP_API_KEY'),
            'units' => 'metric',
            'q' => $cityName,
        ]);
    }
}
