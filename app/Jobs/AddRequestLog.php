<?php

namespace App\Jobs;

use App\Models\RequestLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddRequestLog implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private string $cityName;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $cityName)
    {
        $this->cityName = $cityName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $newRequestLog = new RequestLog();
        $newRequestLog->city = $this->cityName;

        $newRequestLog->save();
    }
}
