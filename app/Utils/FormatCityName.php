<?php

namespace App\Utils;

class FormatCityName
{
    /**
     * Приводит строку в формат Aaaa.
     */
    public static function format(string $cityName): string
    {
        $cityName = mb_strtolower($cityName);
        //Кириллица будь она неладна.
        $cityName = mb_strtoupper(mb_substr($cityName, 0, 1)) . mb_substr($cityName, 1);
        return $cityName;
    }
}
