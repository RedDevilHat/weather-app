<?php

namespace App\Utils;

class GeneratorHelper
{
    public static function genereted(array $data): \Generator
    {
        yield from $data;
    }
}
