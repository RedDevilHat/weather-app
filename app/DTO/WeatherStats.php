<?php

namespace App\DTO;

use JetBrains\PhpStorm\ArrayShape;

/**
 * Базовая обертка для ответов источников информации о погоде.
 */
class WeatherStats
{
    /**
     * Город.
     */
    private string $cityName;
    /**
     * Текущая погода.
     */
    private float $temp;
    /**
     * Источник.
     */
    private string $source;

    /**
     * @param string $cityName
     * @param float $temp
     * @param string $source
     */
    public function __construct(string $cityName, float $temp, string $source)
    {
        $this->cityName = $cityName;
        $this->temp = $temp;
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getCityName(): string
    {
        return $this->cityName;
    }

    /**
     * @return float
     */
    public function getTemp(): float
    {
        return $this->temp;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    #[ArrayShape(['city' => 'string', 'temp' => 'float', 'source' => 'string'])]
 public function toArray(): array
 {
     return [
            'city' => $this->cityName,
            'temp' => $this->temp,
            'source' => $this->source,
        ];
 }
}
