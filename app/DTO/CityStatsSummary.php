<?php

namespace App\DTO;

class CityStatsSummary
{
    /**
     * @var CityStats[]
     */
    private array $stats;

    /**
     * @param CityStats[] $stats
     */
    public function __construct(array $stats)
    {
        $this->stats = $stats;
    }

    /**
     * @return CityStats[]
     */
    public function getStats(): array
    {
        return $this->stats;
    }

    public function toArray(): array
    {
        $data = [];

        foreach ($this->stats as $stat) {
            $data[] = $stat->toArray();
        }

        return [
            'data' => $data,
        ];
    }
}
