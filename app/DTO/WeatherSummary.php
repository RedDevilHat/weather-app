<?php

namespace App\DTO;

use JetBrains\PhpStorm\ArrayShape;

/**
 * Сводная информация по всем системам.
 */
class WeatherSummary
{
    /**
     * Список всех данных по системам.
     * @var WeatherStats[]
     */
    private array $stats;
    /**
     * Средняя температура.
     */
    private float $averagedTemp;

    /**
     * @param array $stats
     * @param float $averagedTemp
     */
    public function __construct(array $stats, float $averagedTemp)
    {
        $this->stats = $stats;
        $this->averagedTemp = $averagedTemp;
    }

    /**
     * @return WeatherStats[]
     */
    public function getStats(): array
    {
        return $this->stats;
    }

    /**
     * @return float
     */
    public function getAveragedTemp(): float
    {
        return $this->averagedTemp;
    }

    #[ArrayShape(['data' => 'array', 'averageTemp' => 'float'])]
 public function toArray(): array
 {
     $data = [];

     foreach ($this->stats as $item) {
         $data[] = $item->toArray();
     }

     return [
            'data' => $data,
            'averageTemp' => $this->averagedTemp,
        ];
 }
}
