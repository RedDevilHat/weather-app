<?php

namespace App\DTO;

class CityStats
{
    private string $cityName;
    private int $countOfRequest;

    /**
     * @param string $cityName
     * @param int $countOfRequest
     */
    public function __construct(string $cityName, int $countOfRequest)
    {
        $this->cityName = $cityName;
        $this->countOfRequest = $countOfRequest;
    }

    /**
     * @return string
     */
    public function getCityName(): string
    {
        return $this->cityName;
    }

    /**
     * @return int
     */
    public function getCountOfRequest(): int
    {
        return $this->countOfRequest;
    }

    public function toArray(): array
    {
        return [
            'city' => $this->cityName,
            'requestCount' => $this->countOfRequest,
        ];
    }
}
