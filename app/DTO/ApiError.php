<?php

namespace App\DTO;

use JetBrains\PhpStorm\ArrayShape;

class ApiError
{
    private string $error;

    /**
     * @param string $error
     */
    public function __construct(string $error)
    {
        $this->error = $error;
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }

    #[ArrayShape(['error' => 'string'])]
 public function toArray(): array
 {
     return ['error' => $this->error];
 }
}
