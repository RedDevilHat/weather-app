<?php

namespace App\Exceptions;

class WeatherParsingException extends \Exception
{
    protected $message = 'Cannot parse weather data';
}
