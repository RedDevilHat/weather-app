<?php

namespace App\Exceptions;

class WeatherGettingException extends \Exception
{
    protected $message = 'Cannot get weather';
}
