<?php

namespace App\Exceptions;

class WeatherNotFoundException extends \Exception
{
    protected $message = 'Not found weather data';
}
