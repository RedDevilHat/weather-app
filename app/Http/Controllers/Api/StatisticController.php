<?php

namespace App\Http\Controllers\Api;

use App\DTO\ApiError;
use App\DTO\CityStatsSummary;
use App\Story\GetCityStats;
use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class StatisticController
{
    private GetCityStats $cityStats;

    public function __construct(GetCityStats $cityStats)
    {
        $this->cityStats = $cityStats;
    }

    public function getForOneDay(Request $request): JsonResponse
    {
        $day = $request->get('date');

        if (!is_string($day)) {
            return response()->json((new ApiError('Provide date params'))->toArray(), 400);
        }

        try {
            $day = Carbon::createFromFormat('Y-m-d', $day);
        } catch (InvalidFormatException $exception) {
            return response()->json((new ApiError('Ensure you use right date format (Y-m-d)'))->toArray(), 400);
        }


        if (!$day) {
            return response()->json((new ApiError('Ensure you use right date format (Y-m-d)'))->toArray(), 400);
        }

        try {
            $stats = $this->cityStats->forOneDay($day);
        } catch (\LogicException $exception) {
            return response()->json((new ApiError($exception->getMessage()))->toArray(), 400);
        } catch (\Throwable $exception) {
            Log::critical(sprintf('[stats] cannot process request'));

            return response()->json((new ApiError('Server error'))->toArray(), 500);
        }


        return response()->json((new CityStatsSummary($stats))->toArray());
    }

    public function getForRange(Request $request): JsonResponse
    {
        $dateStart = $request->get('dateStart');
        $dateEnd = $request->get('dateEnd');

        if (!is_string($dateStart) || !is_string($dateEnd)) {
            return response()->json((new ApiError('Provide date params'))->toArray(), 400);
        }

        try {
            $dateStart = Carbon::createFromFormat('Y-m-d', $dateStart);
            $dateEnd = Carbon::createFromFormat('Y-m-d', $dateEnd);
        } catch (InvalidFormatException $exception) {
            return response()->json((new ApiError('Ensure you use right date format (Y-m-d)'))->toArray(), 400);
        }


        if (!$dateStart || !$dateEnd) {
            return response()->json((new ApiError('Ensure you use right date format (Y-m-d)'))->toArray(), 400);
        }

        try {
            $stats = $this->cityStats->forRange($dateStart, $dateEnd);
        } catch (\LogicException $exception) {
            return response()->json((new ApiError($exception->getMessage()))->toArray(), 400);
        } catch (\Throwable $exception) {
            Log::critical(sprintf('[stats] cannot process request'));

            return response()->json((new ApiError('Server error'))->toArray(), 500);
        }


        return response()->json((new CityStatsSummary($stats))->toArray());
    }
}
