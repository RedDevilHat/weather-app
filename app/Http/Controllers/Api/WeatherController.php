<?php

namespace App\Http\Controllers\Api;

use App\DTO\ApiError;
use App\Exceptions\WeatherNotFoundException;
use App\Jobs\AddRequestLog;
use App\Story\GetWeatherInAllSystem;
use App\Utils\FormatCityName;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;

class WeatherController extends Controller
{
    private GetWeatherInAllSystem $getWeatherInAllSystem;

    public function __construct(GetWeatherInAllSystem $getWeatherInAllSystem)
    {
        $this->getWeatherInAllSystem = $getWeatherInAllSystem;
    }

    public function weather(Request $request): JsonResponse
    {
        $city = $request->get('city');

        if (!is_string($city)) {
            return response()->json((new ApiError('Provide city name'))->toArray(), 400);
        }

        $city = FormatCityName::format($city);

        try {
            $summary = $this->getWeatherInAllSystem->getSummary($city);
            AddRequestLog::dispatchAfterResponse($city);
            return response()->json($summary->toArray());
        } catch (WeatherNotFoundException $exception) {
            Log::warning(sprintf('[city:%s] not found data or data corrupted', $city));

            return response()->json((new ApiError(sprintf('Not found weather for %s', $city)))->toArray(), 404);
        } catch (\Throwable $exception) {
            Log::critical(sprintf('[city:%s] cannot process request', $city));

            return response()->json((new ApiError(sprintf('Not found weather for %s', $city)))->toArray(), 500);
        }
    }
}
