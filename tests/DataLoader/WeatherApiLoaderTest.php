<?php

namespace Tests\DataLoader;

use App\Clients\WeatherApiClient;
use App\DataLoaders\WeatherApiLoader;
use App\Exceptions\WeatherGettingException;
use App\Exceptions\WeatherParsingException;
use Illuminate\Http\Client\Response;
use PHPUnit\Framework\TestCase;

class WeatherApiLoaderTest extends TestCase
{
    public function testLoad()
    {
        $city = 'Test';
        $temp = 0.1;

        $client = $this->createMock(WeatherApiClient::class);
        $response = $this->createMock(Response::class);

        $response->method('json')->willReturn([
            'location' => ['name' => $city],
            'current' => ['temp_c' => $temp],
        ]);

        $client->method('request')->willReturn($response);

        $loader = new WeatherApiLoader($client);
        $answer = $loader->load($city);
        $this->assertEquals($city, $answer->getCityName());
        $this->assertEquals($temp, $answer->getTemp());
        $this->assertEquals(WeatherApiLoader::SOURCE, $answer->getSource());
    }

    public function testFailedAnswer(): void
    {
        $city = 'Test';

        $client = $this->createMock(WeatherApiClient::class);
        $response = $this->createMock(Response::class);

        $response->method('failed')->willReturn(true);

        $client->method('request')->willReturn($response);

        $this->expectException(WeatherGettingException::class);

        $loader = new WeatherApiLoader($client);
        $loader->load($city);
    }

    public function testUnexpectedData(): void
    {
        $city = 'Test';

        $client = $this->createMock(WeatherApiClient::class);
        $response = $this->createMock(Response::class);

        $response->method('json')->willReturn([]);

        $client->method('request')->willReturn($response);

        $this->expectException(WeatherParsingException::class);

        $loader = new WeatherApiLoader($client);
        $loader->load($city);
    }
}
