<?php

namespace Tests\DataLoader;

use App\Clients\M3OApiClient;
use App\DataLoaders\M3OApiLoader;
use App\Exceptions\WeatherGettingException;
use App\Exceptions\WeatherParsingException;
use Illuminate\Http\Client\Response;
use PHPUnit\Framework\TestCase;

class M3OApiLoaderTest extends TestCase
{
    public function testLoad()
    {
        $city = 'Test';
        $temp = 0.1;

        $client = $this->createMock(M3OApiClient::class);
        $response = $this->createMock(Response::class);

        $response->method('json')->willReturn([
            'location' => $city,
            'temp_c' => $temp,
        ]);

        $client->method('request')->willReturn($response);

        $loader = new M3OApiLoader($client);
        $answer = $loader->load($city);
        $this->assertEquals($city, $answer->getCityName());
        $this->assertEquals($temp, $answer->getTemp());
        $this->assertEquals(M3OApiLoader::SOURCE, $answer->getSource());
    }

    public function testFailedAnswer(): void
    {
        $city = 'Test';

        $client = $this->createMock(M3OApiClient::class);
        $response = $this->createMock(Response::class);

        $response->method('failed')->willReturn(true);

        $client->method('request')->willReturn($response);

        $this->expectException(WeatherGettingException::class);

        $loader = new M3OApiLoader($client);
        $loader->load($city);
    }

    public function testUnexpectedData(): void
    {
        $city = 'Test';

        $client = $this->createMock(M3OApiClient::class);
        $response = $this->createMock(Response::class);

        $response->method('json')->willReturn([]);

        $client->method('request')->willReturn($response);

        $this->expectException(WeatherParsingException::class);

        $loader = new M3OApiLoader($client);
        $loader->load($city);
    }
}
