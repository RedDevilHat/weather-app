<?php

namespace Tests\DataLoader;

use App\Clients\OpenWeatherMapApiClient;
use App\DataLoaders\OpenWeatherMapApiLoader;
use App\Exceptions\WeatherGettingException;
use App\Exceptions\WeatherParsingException;
use Illuminate\Http\Client\Response;
use PHPUnit\Framework\TestCase;

class OpenWeatherMapApiLoaderTest extends TestCase
{
    public function testLoad()
    {
        $city = 'Test';
        $temp = 0.1;

        $client = $this->createMock(OpenWeatherMapApiClient::class);
        $response = $this->createMock(Response::class);

        $response->method('json')->willReturn([
            'name' => $city,
            'main' => ['temp' => $temp],
        ]);

        $client->method('request')->willReturn($response);

        $loader = new OpenWeatherMapApiLoader($client);
        $answer = $loader->load($city);
        $this->assertEquals($city, $answer->getCityName());
        $this->assertEquals($temp, $answer->getTemp());
        $this->assertEquals(OpenWeatherMapApiLoader::SOURCE, $answer->getSource());
    }

    public function testFailedAnswer(): void
    {
        $city = 'Test';

        $client = $this->createMock(OpenWeatherMapApiClient::class);
        $response = $this->createMock(Response::class);

        $response->method('failed')->willReturn(true);

        $client->method('request')->willReturn($response);

        $this->expectException(WeatherGettingException::class);

        $loader = new OpenWeatherMapApiLoader($client);
        $loader->load($city);
    }

    public function testUnexpectedData(): void
    {
        $city = 'Test';

        $client = $this->createMock(OpenWeatherMapApiClient::class);
        $response = $this->createMock(Response::class);

        $response->method('json')->willReturn([]);

        $client->method('request')->willReturn($response);

        $this->expectException(WeatherParsingException::class);

        $loader = new OpenWeatherMapApiLoader($client);
        $loader->load($city);
    }
}
