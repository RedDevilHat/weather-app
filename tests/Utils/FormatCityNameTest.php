<?php

namespace Tests\Utils;

use App\Utils\FormatCityName;
use PHPUnit\Framework\TestCase;

class FormatCityNameTest extends TestCase
{
    public function testFormat()
    {
        $input = 'tESt';
        $output = 'Test';

        $this->assertEquals($output, FormatCityName::format($input));
    }
}
