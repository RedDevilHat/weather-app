<?php

namespace Tests\Story;

use App\Clients\M3OApiClient;
use App\Clients\OpenWeatherMapApiClient;
use App\Clients\WeatherApiClient;
use App\DataLoaders\M3OApiLoader;
use App\DataLoaders\OpenWeatherMapApiLoader;
use App\DataLoaders\WeatherApiLoader;
use App\Exceptions\WeatherNotFoundException;
use App\Story\GetWeatherInAllSystem;
use Illuminate\Http\Client\Response;
use Tests\TestCase;

class GetWeatherInAllSystemTest extends TestCase
{
    public function testGetSummary()
    {
        $city = 'Test';
        $temp = 1;

        $clientM30 = $this->createMock(M3OApiClient::class);
        $responseM30 = $this->createMock(Response::class);
        $responseM30->method('json')->willReturn([
            'location' => $city,
            'temp_c' => $temp,
        ]);
        $clientM30->method('request')->willReturn($responseM30);
        $loaderM3O = new M3OApiLoader($clientM30);

        $clientOWM = $this->createMock(OpenWeatherMapApiClient::class);
        $responseOWM = $this->createMock(Response::class);
        $responseOWM->method('json')->willReturn([
            'name' => $city,
            'main' => ['temp' => $temp],
        ]);
        $clientOWM->method('request')->willReturn($responseOWM);
        $loaderOWM = new OpenWeatherMapApiLoader($clientOWM);

        $clientWA = $this->createMock(WeatherApiClient::class);
        $responseWA = $this->createMock(Response::class);
        $responseWA->method('json')->willReturn([
            'location' => ['name' => $city],
            'current' => ['temp_c' => $temp],
        ]);
        $clientWA->method('request')->willReturn($responseWA);
        $loaderWA = new WeatherApiLoader($clientWA);

        $story = new GetWeatherInAllSystem(
            m3OApiLoader: $loaderM3O, openWeatherMapApiLoader: $loaderOWM, weatherApiLoader: $loaderWA
        );

        $summary = $story->getSummary($city);

        $this->assertEquals($summary->getAveragedTemp(), 1);
        $this->assertCount(3, $summary->getStats());
    }

    public function testGetPartialSummary()
    {
        $city = 'Test';
        $temp = 1;

        $clientM30 = $this->createMock(M3OApiClient::class);
        $responseM30 = $this->createMock(Response::class);
        $responseM30->method('json')->willReturn([]);
        $clientM30->method('request')->willReturn($responseM30);
        $loaderM3O = new M3OApiLoader($clientM30);

        $clientOWM = $this->createMock(OpenWeatherMapApiClient::class);
        $responseOWM = $this->createMock(Response::class);
        $responseOWM->method('failed')->willReturn(true);
        $clientOWM->method('request')->willReturn($responseOWM);
        $loaderOWM = new OpenWeatherMapApiLoader($clientOWM);

        $clientWA = $this->createMock(WeatherApiClient::class);
        $responseWA = $this->createMock(Response::class);
        $responseWA->method('json')->willReturn([
            'location' => ['name' => $city],
            'current' => ['temp_c' => $temp],
        ]);
        $clientWA->method('request')->willReturn($responseWA);
        $loaderWA = new WeatherApiLoader($clientWA);

        $story = new GetWeatherInAllSystem(
            m3OApiLoader: $loaderM3O, openWeatherMapApiLoader: $loaderOWM, weatherApiLoader: $loaderWA
        );

        $summary = $story->getSummary($city);

        $this->assertEquals($summary->getAveragedTemp(), 1);
        $this->assertCount(1, $summary->getStats());
    }

    public function testAllFailedSummary()
    {
        $city = 'Test';
        $temp = 1;

        $clientM30 = $this->createMock(M3OApiClient::class);
        $responseM30 = $this->createMock(Response::class);
        $responseM30->method('json')->willReturn([]);
        $clientM30->method('request')->willReturn($responseM30);
        $loaderM3O = new M3OApiLoader($clientM30);

        $clientOWM = $this->createMock(OpenWeatherMapApiClient::class);
        $responseOWM = $this->createMock(Response::class);
        $responseM30->method('json')->willReturn([]);
        $clientOWM->method('request')->willReturn($responseOWM);
        $loaderOWM = new OpenWeatherMapApiLoader($clientOWM);

        $clientWA = $this->createMock(WeatherApiClient::class);
        $responseWA = $this->createMock(Response::class);
        $responseM30->method('json')->willReturn([]);
        $clientWA->method('request')->willReturn($responseWA);
        $loaderWA = new WeatherApiLoader($clientWA);

        $story = new GetWeatherInAllSystem(
            m3OApiLoader: $loaderM3O, openWeatherMapApiLoader: $loaderOWM, weatherApiLoader: $loaderWA
        );

        $this->expectException(WeatherNotFoundException::class);
        $story->getSummary($city);
    }
}
