<?php

namespace Tests\Story;

use App\DataLoaders\ModelLoaders\RequestLogsLoader;
use App\DTO\CityStats;
use App\Story\GetCityStats;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use Tests\DTO\RequestLogsData;

class GetCityStatsTest extends TestCase
{

    public function testForRange()
    {
        $loader = $this->createMock(RequestLogsLoader::class);
        $loader->method('loadByDateRange')->willReturn($this->getSuccessResult());

        $stats = new GetCityStats($loader);

        $dateStart = Carbon::today();
        $dateEnd = Carbon::today();

        $stats = $stats->forRange($dateStart->sub(new \DateInterval('P1M')), $dateEnd);

        $this->assertNotEmpty($stats);

        foreach ($stats as $cityStats) {
            $this->assertInstanceOf(CityStats::class, $cityStats);
        }
    }

    public function testWrongDate(): void
    {
        $loader = $this->createMock(RequestLogsLoader::class);
        $loader->method('loadByDateRange')->willReturn($this->getSuccessResult());

        $stats = new GetCityStats($loader);

        $dateStart = Carbon::today();
        $dateEnd = Carbon::today();

        $this->expectException(\LogicException::class);
        $stats->forRange($dateStart->add(new \DateInterval('P1M')), $dateEnd);
    }

    public function testErrorData(): void
    {
        $loader = $this->createMock(RequestLogsLoader::class);
        $loader->method('loadByDateRange')->willReturn($this->getErrorResult());

        $stats = new GetCityStats($loader);

        $dateStart = Carbon::today();
        $dateEnd = Carbon::today();

        $this->expectException(\LogicException::class);
        $stats->forRange($dateStart->sub(new \DateInterval('P1M')), $dateEnd);
    }

    private function getSuccessResult(): Collection
    {
        $result = [];

        $i = 1;
        while ($i <= 3) {
            $data = new \stdClass();
            $data->city = sprintf('City%d', $i);
            $data->total = $i;

            $result[] = $data;
            ++$i;
        }

        return new Collection($result);
    }

    private function getErrorResult(): Collection
    {
        return new Collection([new \stdClass()]);
    }
}
