<?php

use App\Http\Controllers\Api\StatisticController;
use App\Http\Controllers\Api\WeatherController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/weather', [WeatherController::class, 'weather']);
Route::get('/city-stats/for-one-day', [StatisticController::class, 'getForOneDay']);
Route::get('/city-stats/for-range', [StatisticController::class, 'getForRange']);
